set nocompatible
set nu
syntax on
set encoding=utf-8
set showcmd
filetype plugin indent on
try
        colorscheme desert
catch
endtry
set background=dark
set magic
set tabstop=2
set expandtab
set backspace=indent,eol,start

set hlsearch
set incsearch
set ignorecase
set smartcase
set cursorline
